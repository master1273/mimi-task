object Versions {
    const val min_sdk = 21
    const val target_sdk = 29
    const val compile_sdk = 29

    const val kotlin = "1.3.50"

    const val androidx_appcompat = "1.1.0"
    const val androidx_constraint_layout = "1.1.3"
    const val androidx_ktx = "1.1.0"
    const val androidx_lifecycle = "2.1.0"

    const val navigation_version = "2.1.0"

    const val android_material = "1.0.0"

    const val retrofit = "2.6.2"
    const val retrofit_couroutines_adapter = "0.9.2"

    const val glide = "4.10.0"

    const val exo_player = "2.10.5"
}

object Libs {

    // Kotlin
    const val kotlin_std = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    // androidx
    const val androidx_appcompat = "androidx.appcompat:appcompat:${Versions.androidx_appcompat}"
    const val androidx_ktx = "androidx.core:core-ktx:${Versions.androidx_ktx}"
    const val androidx_constraint_layout ="androidx.constraintlayout:constraintlayout:${Versions.androidx_constraint_layout}"

    const val android_material = "com.google.android.material:material:${Versions.android_material}"

    // view model
    const val lifecycle_extentions = "androidx.lifecycle:lifecycle-extensions:${Versions.androidx_lifecycle}"
    const val lifecycle_view_model = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.androidx_lifecycle}"

    // navigation
    const val navigation_fragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation_version}"
    const val navigation_ui_ktx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofit_gson_converter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofit_couroutines_adapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.retrofit_couroutines_adapter}"

    // Glide
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glide_compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    // Exo player
    const val exo_player = "com.google.android.exoplayer:exoplayer:${Versions.exo_player}"
}