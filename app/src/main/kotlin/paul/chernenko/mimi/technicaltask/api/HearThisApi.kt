package paul.chernenko.mimi.technicaltask.api

import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.model.Song
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface HearThisApi {

    @GET("feed/?page=1&count=5&type=popular")
    suspend fun getPopularSongs(
        @Query("page") page: Int,
        @Query("count") count: Int = 20,
        @Query("type") type: String = "popular"
    ): Response<List<Song>>

    @GET("{artist_name}")
    suspend fun getArtistSongs(
        @Path("artist_name") artistName: String,
        @Query("type") type: String = "tracks",
        @Query("page") page: Int,
        @Query("count") count: Int = 20
    ): Response<List<Song>>

    @GET("{artist_name}")
    suspend fun getArtistDetails(
        @Path("artist_name") artistName: String
    ): Response<Artist>

    @GET
    suspend fun getWaveformData(
        @Url waveformDataUrl: String
    ): Response<List<Int>>

}