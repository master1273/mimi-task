package paul.chernenko.mimi.technicaltask.repository

import android.content.Context
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.api.HearThisApi
import paul.chernenko.mimi.technicaltask.exception.ApiException
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.model.Song

interface HearThisRepository{
    suspend fun getPopularArtists(page: Int): List<Artist>
    suspend fun getArtistDetails(artist: Artist): Artist
    suspend fun getArtistSongs(artist: Artist, page: Int): List<Song>?
    suspend fun getWaveformData(url: String): List<Int>
}

class HearThisRepositoryImpl(private val api: HearThisApi, private val context: Context): BaseRepository(), HearThisRepository {

    @Throws(ApiException::class)
    override suspend fun getPopularArtists(page: Int): List<Artist>{
        val popularSongs = safeApiCall(
            call = {
                api.getPopularSongs(page = page)
            },
            errorMessage = context.resources.getString(R.string.popular_artists_failure)
        ).orEmpty()

        return popularSongs.map { it.artist }.distinctBy { it.id }
    }

    @Throws(ApiException::class)
    override suspend fun getArtistDetails(artist: Artist): Artist {
        return safeApiCall(
            call = {
                api.getArtistDetails(artistName = artist.urlPath)
            },
            errorMessage = context.resources.getString(R.string.artist_details_failure)
        ) ?: artist
    }

    @Throws(ApiException::class)
    override suspend fun getArtistSongs(artist: Artist, page: Int): List<Song>?{
        return safeApiCall(
            call = {
                api.getArtistSongs(artistName = artist.urlPath, page = page)
            },
            errorMessage = context.resources.getString(R.string.artist_details_failure)
        )
    }

    @Throws(ApiException::class)
    override suspend fun getWaveformData(url: String): List<Int> {
        return safeApiCall(
            call = {
                api.getWaveformData(url)
            },
            errorMessage = context.resources.getString(R.string.waveform_data_failure)
        ).orEmpty()
    }
}