package paul.chernenko.mimi.technicaltask.utils

import android.util.TypedValue
import android.view.View

fun Int.convertToPx(view: View): Int {
    val dm = view.resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), dm).toInt()
}