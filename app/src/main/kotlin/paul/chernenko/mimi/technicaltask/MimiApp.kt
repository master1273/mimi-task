package paul.chernenko.mimi.technicaltask

import android.app.Application
import android.content.Context
import paul.chernenko.mimi.technicaltask.api.ApiClient
import paul.chernenko.mimi.technicaltask.api.HearThisApi

class MimiApp : Application() {

    companion object {
        @JvmStatic lateinit var instance: MimiApp
            private set

        @JvmStatic lateinit var apiClient: HearThisApi

        @JvmStatic val appContext : Context
            get() = instance.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        apiClient = ApiClient.getInstance()
    }

}