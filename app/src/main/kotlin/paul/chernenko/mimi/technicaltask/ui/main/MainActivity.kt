package paul.chernenko.mimi.technicaltask.ui.main

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.main_activity.*
import paul.chernenko.mimi.technicaltask.MimiApp
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.PlaybackProgressData
import paul.chernenko.mimi.technicaltask.model.Song
import paul.chernenko.mimi.technicaltask.repository.HearThisRepositoryImpl
import paul.chernenko.mimi.technicaltask.service.MusicPlaybackService
import paul.chernenko.mimi.technicaltask.ui.getViewModel
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult
import paul.chernenko.mimi.technicaltask.utils.TimeUtils
import paul.chernenko.mimi.technicaltask.utils.isServiceRunning

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by lazy {
        getViewModel{ MainViewModel(HearThisRepositoryImpl(MimiApp.apiClient, MimiApp.appContext)) }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            viewModel.serviceConnected(binder as MusicPlaybackService.LocalBinder)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            viewModel.serviceDisconnected()
        }
    }

    private val titleObserver = Observer<String> {title ->
        title_text.text = title
    }

    private val progressObserver = Observer<PlaybackProgressData> { progressData ->
        if (waveform.visibility == View.VISIBLE) {
            waveform.setProgress(progressData.getProgressPercentage())
            waveform.setSecondaryProgress(progressData.getBufferingProgressPercentage())
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progress_bar.setProgress(progressData.getProgressPercents(), true)
            } else {
                progress_bar.progress = progressData.getProgressPercents()
            }
            progress_bar.secondaryProgress = progressData.getBufferingProgressPercents()
        }


        progress_text.text = TimeUtils.millisecondsToStringRepresentation(progressData.progress)
        duration_text.text = TimeUtils.millisecondsToStringRepresentation(progressData.duration)
    }

    private val playbackControlsObserver = Observer<Boolean> { visible ->

        if (visible && player_controls.visibility == View.GONE) {
            player_controls.visibility = View.VISIBLE
        }

        if (!visible && player_controls.visibility == View.VISIBLE) {
            player_controls.visibility = View.GONE
            waveform.visibility = View.INVISIBLE
            progress_bar.visibility = View.VISIBLE
        }
    }

    private val waveformDataObserver = Observer<LiveDataResult<List<Int>>> { result ->
        if (result.status == LiveDataResult.Status.SUCCESS){
            waveform.setColors(
                progressColor = ContextCompat.getColor(this@MainActivity, R.color.seekbarProgress),
                secondaryProgressColor = ContextCompat.getColor(this@MainActivity, R.color.seekbarSecondaryProgress),
                inactiveColor = ContextCompat.getColor(this@MainActivity, R.color.seekbarBackground)
            )
            waveform.setData(result.data!!)
            waveform.setProgress(0F)
            waveform.setSecondaryProgress(0F)
            waveform.visibility = View.VISIBLE
            progress_bar.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        viewModel.progressObserver.observe(this, progressObserver)
        viewModel.playbackControlsObserver.observe(this, playbackControlsObserver)
        viewModel.titleObserver.observe(this, titleObserver)
        viewModel.waveformDataObserver.observe(this, waveformDataObserver)

        viewModel.displayOrHidePlaybackControls()

        configurePlaybackControls()
    }

    override fun onStart() {
        super.onStart()
        if (isServiceRunning(this, MusicPlaybackService::class.java)){
            bindMusicService(serviceConnection)
        }
        viewModel.displayOrHidePlaybackControls()
    }

    override fun onStop() {
        super.onStop()
        if (isServiceRunning(this, MusicPlaybackService::class.java)){
            unbindService(serviceConnection)
        }
        viewModel.displayOrHidePlaybackControls()
    }

    private fun bindMusicService(connection: ServiceConnection) {
        val intent = Intent(this, MusicPlaybackService::class.java)
        bindService(intent, connection, 0)
    }

    fun startSongPlayback(song: Song) {
        if (isServiceRunning(this, MusicPlaybackService::class.java)){
            unbindService(serviceConnection)
            MusicPlaybackService.stop(this)
        }
        MusicPlaybackService.start(this, song)
        bindMusicService(serviceConnection)
    }

    private fun configurePlaybackControls(){
        close.setOnClickListener {
            MusicPlaybackService.stop(this)
        }
    }
}
