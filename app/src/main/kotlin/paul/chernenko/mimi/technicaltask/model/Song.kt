package paul.chernenko.mimi.technicaltask.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Song (
    @SerializedName("id")
    val id: Int,

    @SerializedName("duration")
    val duration: Long,

    @SerializedName("genre")
    val genre: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("artwork_url")
    val artworkUrl: String,

    @SerializedName("waveform_data")
    val waveformDataUrl: String,

    @SerializedName("user")
    val artist: Artist,

    @SerializedName("stream_url")
    val streamUrl: String
): Parcelable