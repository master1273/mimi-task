package paul.chernenko.mimi.technicaltask.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    private var retrofit: Retrofit? = null

    private const val BASE_URL = "https://api-v2.hearthis.at/"

    fun getInstance(): HearThisApi {
        if (retrofit == null)
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(createClient())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                .build()
        return retrofit!!.create<HearThisApi>(
            HearThisApi::class.java)
    }

    private fun createGson(): Gson {
        return GsonBuilder()
            .setDateFormat("MM/dd/yyyy")
            .create()
    }

    private fun createClient(): OkHttpClient {
        return OkHttpClient().newBuilder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}