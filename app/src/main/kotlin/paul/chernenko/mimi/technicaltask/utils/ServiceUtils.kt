package paul.chernenko.mimi.technicaltask.utils

import android.app.ActivityManager
import android.app.Service
import android.content.Context

@Suppress("DEPRECATION")
fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
    val manager =  context.getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager?
    manager?.getRunningServices(Integer.MAX_VALUE).orEmpty().forEach {
        if (serviceClass.name == it.service.className) {
            return true
        }
    }
    return false
}