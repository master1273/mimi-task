package paul.chernenko.mimi.technicaltask.utils

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util

fun SimpleExoPlayer.prepareToPlayAudioFromUrl(context: Context, url: String, cache: Cache? = null) {
    val factory = if (cache != null)
        CacheDataSourceFactory(cache, DefaultHttpDataSourceFactory(ExoPlayerEx.getUserAgent(context)))
    else
        DefaultDataSourceFactory(context, ExoPlayerEx.getUserAgent(context))
    val mediaSource = ProgressiveMediaSource.Factory(factory).createMediaSource(Uri.parse(url))
    prepare(mediaSource)
}

object ExoPlayerEx {
    @JvmStatic
    fun getUserAgent(context: Context): String {
        val packageManager = context.packageManager
        val info = packageManager.getPackageInfo(context.packageName, 0)
        val appName = info.applicationInfo.loadLabel(packageManager).toString()
        return Util.getUserAgent(context, appName)
    }

    @JvmStatic
    fun convertPlaybackStateToString(playbackState: Int?): String {
        return when (playbackState) {
            Player.STATE_IDLE -> "STATE_IDLE"
            Player.STATE_BUFFERING -> "STATE_BUFFERING"
            Player.STATE_READY -> "STATE_READY"
            Player.STATE_ENDED -> "STATE_ENDED"
            else -> "unknown"
        }
    }
}