package paul.chernenko.mimi.technicaltask.exception

import java.lang.Exception

class ApiException(override val message: String): Exception()