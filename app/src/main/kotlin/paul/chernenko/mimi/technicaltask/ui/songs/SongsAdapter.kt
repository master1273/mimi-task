package paul.chernenko.mimi.technicaltask.ui.songs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_song.view.*
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.Song
import paul.chernenko.mimi.technicaltask.utils.TimeUtils

class SongsAdapter(
    private val items: MutableList<Song>,
    private val onSongSelected: (song: Song) -> Unit
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoaderVisible = false

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_VIEW) {
             SongsViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.item_song,
                    viewGroup,
                    false
                )
            )
        } else {
            ProgressViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.item_progress,
                    viewGroup,
                    false))
        }
    }

    override fun getItemCount(): Int = if (!isLoaderVisible) items.size else items.size + 1

    override fun getItemViewType(position: Int): Int {
        return if (position == items.size) VIEW_TYPE_LOADING else VIEW_TYPE_VIEW
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SongsViewHolder) {
            holder.setupItem(items[position], onSongSelected)
        }
    }

    fun addItems(newList: List<Song>){
        removeLoadingIndicator()
        val newStartPosition = items.size
        items.addAll(newList)
        notifyItemRangeInserted(newStartPosition, newList.size)
    }

    fun showLoadingIndicator() {
        if (!isLoaderVisible) {
            isLoaderVisible = true
            notifyItemInserted(items.size)
        }
    }

    fun removeLoadingIndicator() {
        if (isLoaderVisible) {
            isLoaderVisible = false
            notifyItemRemoved(items.size)
        }
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class SongsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun setupItem(song: Song, onSongSelected: (song: Song) -> Unit){
            itemView.apply {
                song_name.text = song.title
                genre.text = song.genre


                duration.text = TimeUtils.secondsToStringRepresentation(song.duration)

                Glide
                    .with(itemView)
                    .load(song.artworkUrl)
                    .centerInside()
                    .placeholder(R.drawable.ic_song_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(artwork)

                setOnClickListener { onSongSelected(song) }
            }
        }


    }

    companion object{
        private const val VIEW_TYPE_VIEW = 0
        private const val VIEW_TYPE_LOADING = 1
    }
}