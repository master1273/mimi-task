package paul.chernenko.mimi.technicaltask.ui.artists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_artist.view.*
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.utils.BaseDiffUtilsCallback

class ArtistsAdapter(
    private val items : MutableList<Artist>,
    private val onArtistSelected: (artist: Artist) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoaderVisible = false

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_VIEW) {
            ArtistViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_artist, viewGroup, false))
        } else {
            ProgressViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_progress, viewGroup, false))
        }
    }

    override fun getItemCount(): Int = if (!isLoaderVisible) items.size else items.size + 1

    override fun getItemViewType(position: Int): Int {
        return if (position == items.size) VIEW_TYPE_LOADING else VIEW_TYPE_VIEW
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArtistViewHolder) {
            holder.setupItem(items[position], onArtistSelected)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (holder is ArtistViewHolder && payloads.isNotEmpty()) {

            payloads.forEach {
                when(it){
                    Payload.LOCATION -> holder.setupLocation(items[position].location)
                    Payload.TRACKS_COUNT -> holder.setupTracksCount(items[position].trackCount)
                }
            }
        }

        super.onBindViewHolder(holder, position, payloads)
    }

    fun showLoadingIndicator() {
        if (!isLoaderVisible) {
            isLoaderVisible = true
            notifyItemInserted(items.size)
        }
    }

    fun removeLoadingIndicator() {
        if (isLoaderVisible) {
            isLoaderVisible = false
            notifyItemRemoved(items.size)
        }
    }

    fun replaceItems(newItems: List<Artist>){

        removeLoadingIndicator()

        val diffUtilCallback = BaseDiffUtilsCallback(items, newItems)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback, true)
        diffResult.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(newItems)
    }

    fun updateArtistDetails(artist: Artist){

        val artistToUpdate = items.find { it.id == artist.id } ?: return

        val index = items.indexOf(artistToUpdate)

        if (index == -1) {
            return
        }

        items[index].apply {
            trackCount = artist.trackCount
            location = artist.location
        }
        notifyItemChanged(index, listOf(Payload.LOCATION, Payload.TRACKS_COUNT))
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ArtistViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun setupItem(artist: Artist, onArtistSelected: (artist: Artist) -> Unit) {

            itemView.apply {
                username.text = artist.username

                setupLocation(artist.location)
                setupTracksCount(artist.trackCount)

                Glide
                    .with(itemView)
                    .load(artist.avatarUrl)
                    .centerInside()
                    .placeholder(R.drawable.ic_artist_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(avatar)

                setOnClickListener { onArtistSelected(artist) }
            }
        }

        fun setupTracksCount(tracksCount: Int?){
            val trackCountText = "${itemView.context.resources.getString(R.string.tracks_count)}: ${tracksCount ?: "N/A"}"
            itemView.tracks_count.text = trackCountText
        }

        fun setupLocation(locationText: String?){
            itemView.apply {
                if (location != null) {
                    location.visibility = View.VISIBLE
                    location.text = locationText
                } else {
                    location.visibility = View.GONE
                }
            }
        }
    }

    enum class Payload{
        TRACKS_COUNT, LOCATION
    }

    companion object{
        private const val VIEW_TYPE_VIEW = 0
        private const val VIEW_TYPE_LOADING = 1
    }
}