package paul.chernenko.mimi.technicaltask.ui.songs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import paul.chernenko.mimi.technicaltask.exception.ApiException
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.model.Song
import paul.chernenko.mimi.technicaltask.repository.HearThisRepository
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult

class SongsViewModel(private val hearThisRepository: HearThisRepository) : ViewModel() {

    private var nextPageToLoad = 1
    private var shouldLoadNext = true

    val songsObserver = MutableLiveData<LiveDataResult<List<Song>>>()
    val loadingLiveData = MutableLiveData<Boolean>()
    lateinit var artist: Artist

    /**
     * Fetch artist songs
     */
    fun fetchSongs(){
        if (shouldLoadNext){
            viewModelScope.coroutineContext.cancelChildren()
            viewModelScope.launch {
                songsObserver.postValue(LiveDataResult.loading())
                try {
                    val newSongs = hearThisRepository.getArtistSongs(artist, nextPageToLoad)

                    if (newSongs.isNullOrEmpty()){
                        setLoadingVisibility(false)
                        shouldLoadNext = false
                    } else {
                        nextPageToLoad++
                        setLoadingVisibility(false)
                        songsObserver.postValue(LiveDataResult.success(newSongs))
                    }
                } catch (apiException: ApiException) {
                    setLoadingVisibility(false)
                    songsObserver.postValue(LiveDataResult.error(apiException))
                }
            }
        }
    }

    /**
     * Set a progress dialog visible on the View
     * @param visible visible or not visible
     */
    fun setLoadingVisibility(visible: Boolean) {
        loadingLiveData.postValue(visible)
    }

}
