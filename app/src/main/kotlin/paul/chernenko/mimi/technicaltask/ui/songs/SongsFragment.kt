package paul.chernenko.mimi.technicaltask.ui.songs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_songs.*
import paul.chernenko.mimi.technicaltask.MimiApp
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.model.Song
import paul.chernenko.mimi.technicaltask.repository.HearThisRepositoryImpl
import paul.chernenko.mimi.technicaltask.service.MusicPlaybackService
import paul.chernenko.mimi.technicaltask.ui.getViewModel
import paul.chernenko.mimi.technicaltask.ui.main.MainActivity
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult

class SongsFragment : Fragment() {

    val viewModel: SongsViewModel by lazy {
        getViewModel{ SongsViewModel(HearThisRepositoryImpl(MimiApp.apiClient, MimiApp.appContext)) }
    }

    // Flag to allow fetch songs when the end of the list is reached
    // Depends on the loading state
    private var lockListView: Boolean = false

    private val songsObserver = Observer<LiveDataResult<List<Song>>> { result ->
        when (result?.status) {
            LiveDataResult.Status.LOADING -> {
                this.viewModel.setLoadingVisibility(true)
                lockListView = true
            }

            LiveDataResult.Status.ERROR -> {
                showErrorSnackbar(result.err!!.message!!)
                lockListView = true
            }

            LiveDataResult.Status.SUCCESS -> {
                addListItems(result.data!!)
                lockListView = false
            }
        }
    }

    private val loadingObserver = Observer<Boolean> { visible ->
        if (visible) {
            (songs_list.adapter as? SongsAdapter)?.apply {
                showLoadingIndicator()
                songs_list.smoothScrollToPosition(itemCount - 1)
            }
        } else {
            (songs_list.adapter as? SongsAdapter)?.removeLoadingIndicator()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_songs, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // get artist
        val artist: Artist = arguments?.getParcelable("artist")!!
        viewModel.artist = artist

        // bind observers
        this.viewModel.songsObserver.observe(this, this.songsObserver)
        this.viewModel.loadingLiveData.observe(this, this.loadingObserver)

        // configure recycler view
        configureList()

        // Load data
        this.viewModel.fetchSongs()

        // set toolbar title
        (activity as? AppCompatActivity)?.supportActionBar?.title = artist.username
    }

    private fun showErrorSnackbar(message: String){
        Snackbar.make(main, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.try_again){
                this.viewModel.fetchSongs()
            }
            .show()
    }

    private fun configureList(list: MutableList<Song> = mutableListOf()){
        songs_list.layoutManager = LinearLayoutManager(activity)
        songs_list.setHasFixedSize(true)
        songs_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && !lockListView) {
                    viewModel.fetchSongs()
                }
            }
        })
        songs_list.adapter = SongsAdapter(list){ song ->
            (activity as MainActivity).startSongPlayback(song)
        }
    }

    private fun addListItems(songs: List<Song>){
        if ((songs_list.adapter as? SongsAdapter) != null) {
            (songs_list.adapter as SongsAdapter).addItems(songs)
        } else {
            configureList(songs.toMutableList())
        }
    }

}
