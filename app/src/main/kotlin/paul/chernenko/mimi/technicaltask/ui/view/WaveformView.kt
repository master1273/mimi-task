package paul.chernenko.mimi.technicaltask.ui.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import paul.chernenko.mimi.technicaltask.utils.convertToPx
import kotlin.math.min

class WaveformView(context: Context, attrs: AttributeSet): View(context, attrs) {

    private val drawer = WaveformDrawer(this)

    private var progressValueAnimator: ValueAnimator? = null
    private var progressAnimatorValue = 0f

    private var secondaryProgressValueAnimator: ValueAnimator? = null
    private var secondaryProgressAnimatorValue = 0f

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        drawer.onSizeChanged()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val resolvedWidthSpec = resolveMeasureSpec(widthMeasureSpec,
            DP_DEFAULT_WIDTH
        )
        val resolvedHeightSpec = resolveMeasureSpec(heightMeasureSpec,
            DP_DEFAULT_HEIGHT
        )
        super.onMeasure(resolvedWidthSpec, resolvedHeightSpec)
    }

    private fun resolveMeasureSpec(measureSpec: Int, dpDefault: Int): Int {
        val mode = MeasureSpec.getMode(measureSpec)
        if (mode == MeasureSpec.EXACTLY) {
            return measureSpec
        }

        var defaultSize = dpDefault.convertToPx(this)
        if (mode == MeasureSpec.AT_MOST) {
            defaultSize = min(defaultSize, MeasureSpec.getSize(measureSpec))
        }

        return MeasureSpec.makeMeasureSpec(defaultSize, MeasureSpec.EXACTLY)
    }

    override fun onDraw(canvas: Canvas) {
        drawer.onDraw(canvas)
    }

    fun setColors(progressColor: Int, secondaryProgressColor: Int, inactiveColor: Int){
        drawer.progressColor = progressColor
        drawer.secondaryProgressColor = secondaryProgressColor
        drawer.inactiveColor = inactiveColor
        invalidate()
    }

    fun setData(data: List<Int>){
        drawer.setBars(data)
        progressAnimatorValue = 0f
        secondaryProgressAnimatorValue = 0f
        invalidate()
    }

    /**
     * @param progress progress percentage value
     */
    fun setProgress(progress: Float){
        if (progressValueAnimator != null) {
            progressValueAnimator!!.cancel()
        }

        progressValueAnimator = ValueAnimator.ofFloat(progressAnimatorValue, progress).apply {
            interpolator = LinearInterpolator()
            duration = 1000
            addUpdateListener {
                progressAnimatorValue = it.animatedValue as Float
                drawer.progress = progressAnimatorValue
                invalidate()
            }
            start()
        }
    }

    /**
     * @param progress progress percentage value
     */
    fun setSecondaryProgress(progress: Float){
        if (secondaryProgressValueAnimator != null) {
            secondaryProgressValueAnimator!!.cancel()
        }

        secondaryProgressValueAnimator = ValueAnimator.ofFloat(secondaryProgressAnimatorValue, progress).apply {
            interpolator = LinearInterpolator()
            duration = 1000
            addUpdateListener {
                secondaryProgressAnimatorValue = it.animatedValue as Float
                drawer.secondaryProgress = secondaryProgressAnimatorValue
                invalidate()
            }
            start()
        }
    }

    companion object {
        private const val DP_DEFAULT_WIDTH = 200
        private const val DP_DEFAULT_HEIGHT = 32
    }

}