package paul.chernenko.mimi.technicaltask.ui.view

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

class WaveformDrawer(private val view: WaveformView) {

    private var viewportHeight: Int = 0
    private var viewportWidth: Int = 0

    private var barMaxRelativeValue = 0
    private var barWidth = 0f
    private var bars = emptyList<Int>()

    var progressColor = Color.DKGRAY
    var secondaryProgressColor = Color.GRAY
    var inactiveColor = Color.LTGRAY

    var progress = 0F
    var secondaryProgress = 0f

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    fun onSizeChanged() {
        viewportHeight = view.height - view.paddingTop - view.paddingBottom
        viewportWidth = view.width - view.paddingStart - view.paddingEnd
        configureSizes()
    }

    fun setBars(bars: List<Int>) {
        this.bars = bars
        this.barMaxRelativeValue = bars.max() ?: 0
        configureSizes()
    }

    private fun configureSizes(){
        barWidth = if (bars.isEmpty()) {
            0F
        } else {
            viewportWidth.toFloat() / bars.size.toFloat()
        }
    }

    fun onDraw(canvas: Canvas) {
        bars.forEachIndexed { index, barValue ->
            val barHeight = barValue.toFloat() /
                    (if (barMaxRelativeValue > 0) barMaxRelativeValue.toFloat() else barValue.toFloat()) *
                    viewportHeight
            val xPosition = barWidth * index
            val yPosition = viewportHeight.toFloat() / 2f - barHeight / 2

            paint.strokeWidth = barWidth
            paint.color = if (index <= progress * bars.size) {
                progressColor
            } else if (index > progress * bars.size && index <= secondaryProgress * bars.size){
                secondaryProgressColor
            } else {
                inactiveColor
            }

            canvas.drawLine(xPosition, yPosition, xPosition, yPosition + barHeight, paint)
        }
    }
}