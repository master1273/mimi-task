package paul.chernenko.mimi.technicaltask.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Artist (
    @SerializedName("id")
    val id: Int,

    @SerializedName("username")
    val username: String,

    @SerializedName("permalink")
    val urlPath: String,

    @SerializedName("avatar_url")
    val avatarUrl: String,

    @SerializedName("track_count")
    var trackCount: Int?,

    @SerializedName("geo")
    var location: String?
): Parcelable