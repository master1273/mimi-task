package paul.chernenko.mimi.technicaltask.ui.artists

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import paul.chernenko.mimi.technicaltask.exception.ApiException
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.repository.HearThisRepository
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult

class ArtistsViewModel(private val hearThisRepository: HearThisRepository) : ViewModel() {

    private val loadedArtistsList = mutableListOf<Artist>()
    private var nextPageToLoad = 1
    private var shouldLoadNext = true

    private var savedScrollPosition: Int? = null

    val artistsObserver = MutableLiveData<LiveDataResult<List<Artist>>>()
    val selectedArtistPositionObserver = MutableLiveData<Int>()
    val artistDetailsObserver = MutableLiveData<Artist>()
    val loadingLiveData = MutableLiveData<Boolean>()

    /**
     * Fetch popular artists
     */
    fun fetchArtists(){
        if (savedScrollPosition != null && loadedArtistsList.isNotEmpty()) {
            artistsObserver.postValue(LiveDataResult.success(loadedArtistsList))
            selectedArtistPositionObserver.postValue(savedScrollPosition)
            savedScrollPosition = null
        } else if (shouldLoadNext) {
            viewModelScope.coroutineContext.cancelChildren()
            viewModelScope.launch {
                artistsObserver.postValue(LiveDataResult.loading())
                try {
                    val newPopularArtists = hearThisRepository.getPopularArtists(nextPageToLoad)

                    if (newPopularArtists.isEmpty()) {
                        setLoadingVisibility(false)
                        loadArtistDetails()
                        shouldLoadNext = false
                    } else {
                        newPopularArtists.forEach {  newArtist ->
                            if (loadedArtistsList.find { it.id == newArtist.id } == null){
                                loadedArtistsList.add(newArtist)
                            }
                        }

                        nextPageToLoad++

                        setLoadingVisibility(false)
                        artistsObserver.postValue(LiveDataResult.success(loadedArtistsList))
                    }
                } catch (apiException: ApiException) {
                    setLoadingVisibility(false)
                    artistsObserver.postValue(LiveDataResult.error(apiException))
                }
            }
        }
    }

    /**
     * Fetch artist details
     */
    fun loadArtistDetails(){
        viewModelScope.launch {
            val listToLoad = loadedArtistsList.filter { it.location == null && it.trackCount == null }.reversed()
            listToLoad.forEach {  artist ->
                if (artist.location == null && artist.trackCount == null) {

                    try {
                        val details = hearThisRepository.getArtistDetails(artist)
                        loadedArtistsList.find { it.id == artist.id }?.apply {
                            trackCount = details.trackCount
                            location = details.location
                        }
                        artistDetailsObserver.postValue(details)
                    } catch (ignored: ApiException) { }
                }
            }
        }
    }

    fun saveScrollPosition(position: Int){
        savedScrollPosition = position
    }

    /**
     * Set a progress dialog visible on the View
     * @param visible visible or not visible
     */
    fun setLoadingVisibility(visible: Boolean) {
        loadingLiveData.postValue(visible)
    }

}
