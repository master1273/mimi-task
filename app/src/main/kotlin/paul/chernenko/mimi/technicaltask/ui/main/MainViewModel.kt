package paul.chernenko.mimi.technicaltask.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import paul.chernenko.mimi.technicaltask.exception.ApiException
import paul.chernenko.mimi.technicaltask.model.PlaybackProgressData
import paul.chernenko.mimi.technicaltask.repository.HearThisRepository
import paul.chernenko.mimi.technicaltask.service.MusicPlaybackService
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult

class MainViewModel(private val hearThisRepository: HearThisRepository) : ViewModel(){

    private var musicService : MusicPlaybackService.LocalBinder? = null

    val progressObserver = MutableLiveData<PlaybackProgressData>()
    val playbackControlsObserver = MutableLiveData<Boolean>()
    val titleObserver = MutableLiveData<String>()
    val waveformDataObserver = MutableLiveData<LiveDataResult<List<Int>>>()

    fun serviceConnected(musicService: MusicPlaybackService.LocalBinder){
        this.musicService = musicService
        configureMusicService()
        displayOrHidePlaybackControls()
    }

    fun serviceDisconnected(){
        this.musicService = null
        displayOrHidePlaybackControls()
        progressObserver.postValue(PlaybackProgressData(0, 0, 1))
        titleObserver.postValue("")
    }

    private fun configureMusicService(){
        musicService?.setPlaybackProgressListener { progressData ->
            progressObserver.postValue(progressData)
        }

        musicService?.getSong{ song ->
            titleObserver.postValue(song?.title ?: "")
        }
    }

    fun displayOrHidePlaybackControls(){
        val playbackActive = musicService != null
        playbackControlsObserver.postValue(playbackActive)
        if (playbackActive) {
            musicService!!.getSong { song ->
                song?.let {
                    waveformDataObserver.postValue(LiveDataResult.loading())
                    viewModelScope.launch {
                        try {
                            val data = hearThisRepository.getWaveformData(song.waveformDataUrl)
                            waveformDataObserver.postValue(LiveDataResult.success(data))
                        } catch (exception: ApiException){
                            waveformDataObserver.postValue(LiveDataResult.error(exception))
                        }
                    }
                }
            }

        }
    }
}