package paul.chernenko.mimi.technicaltask.repository

import android.util.Log
import paul.chernenko.mimi.technicaltask.api.Result
import paul.chernenko.mimi.technicaltask.exception.ApiException
import retrofit2.Response
import java.io.IOException

open class BaseRepository{

    @Throws(ApiException::class)
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val result : Result<T> = safeApiResult(call,errorMessage)
        var data : T? = null

        when(result) {
            is Result.Success ->
                data = result.data
            is Result.Error -> {
                throw ApiException(errorMessage)
            }
        }


        return data
    }

    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>, errorMessage: String) : Result<T> {
        val response = call.invoke()
        if(response.isSuccessful) return Result.Success(response.body()!!)

        return Result.Error(errorMessage)
    }
}