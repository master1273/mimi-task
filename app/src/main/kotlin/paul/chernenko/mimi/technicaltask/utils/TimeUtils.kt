package paul.chernenko.mimi.technicaltask.utils

import java.util.concurrent.TimeUnit

object TimeUtils {
    fun secondsToStringRepresentation(durationInSeconds: Long): String{

        val hours = TimeUnit.SECONDS.toHours(durationInSeconds)
        val minutes = TimeUnit.SECONDS.toMinutes(durationInSeconds) % TimeUnit.HOURS.toMinutes(1)
        val seconds = durationInSeconds % TimeUnit.MINUTES.toSeconds(1)

        return (if (hours != 0L) "${hours.toString().padStart(2, '0')}:" else "") +
                "${minutes.toString().padStart(2, '0')}:" +
                seconds.toString().padStart(2, '0')
    }

    fun millisecondsToStringRepresentation(durationInMilliseconds: Long): String{

        val hours = TimeUnit.MILLISECONDS.toHours(durationInMilliseconds)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(durationInMilliseconds) % TimeUnit.HOURS.toMinutes(1)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(durationInMilliseconds) % TimeUnit.MINUTES.toSeconds(1)

        return (if (hours != 0L) "${hours.toString().padStart(2, '0')}:" else "") +
                "${minutes.toString().padStart(2, '0')}:" +
                seconds.toString().padStart(2, '0')
    }
}