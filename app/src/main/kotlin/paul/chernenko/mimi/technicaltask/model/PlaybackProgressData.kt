package paul.chernenko.mimi.technicaltask.model

data class PlaybackProgressData (
    val progress: Long,
    val bufferingProgress: Long,
    val duration: Long
){
    fun getProgressPercentage(): Float{
        return progress.toFloat() / duration.toFloat()
    }

    fun getBufferingProgressPercentage(): Float{
        return bufferingProgress.toFloat() / duration.toFloat()
    }

    fun getProgressPercents(): Int{
        return (progress.toFloat() / duration.toFloat() * 100).toInt()
    }

    fun getBufferingProgressPercents(): Int{
        return (bufferingProgress.toFloat() / duration.toFloat() * 100).toInt()
    }
}