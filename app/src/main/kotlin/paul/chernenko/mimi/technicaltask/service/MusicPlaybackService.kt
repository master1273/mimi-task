package paul.chernenko.mimi.technicaltask.service

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.Song
import paul.chernenko.mimi.technicaltask.utils.NotificationUtils
import paul.chernenko.mimi.technicaltask.utils.isServiceRunning
import paul.chernenko.mimi.technicaltask.utils.prepareToPlayAudioFromUrl
import android.app.NotificationManager
import android.os.Binder
import java.io.File
import android.os.Handler
import paul.chernenko.mimi.technicaltask.model.PlaybackProgressData

class MusicPlaybackService: Service() {

    companion object {
        private const val MAX_CACHE_SIZE_IN_BYTES = 15L * 1024L * 1024L
        private const val SONG = "song_key"
        private const val NOTIFICATION_ID = 1759

        fun start(context: Context, song: Song) {
            val intent = Intent(context, MusicPlaybackService::class.java)
            intent.putExtra(SONG, song)
            context.startService(intent)
        }

        fun stop(context: Context) {
            if (isServiceRunning(context, MusicPlaybackService::class.java)) {
                context.stopService(Intent(context, MusicPlaybackService::class.java))
            }
        }
    }

    private var player: SimpleExoPlayer? = null
    private var song: Song? = null
    private var notificationBuilder: Notification.Builder? = null
    private var cache: SimpleCache? = null

    private val progressHandler = Handler()
    private val updateProgressAction = Runnable { updateProgressBar() }

    private var playbackProgressListener: ((progressData: PlaybackProgressData) -> Unit)? = null

    inner class LocalBinder : Binder() {

        fun setPlaybackProgressListener(playbackProgressListener: (progressData: PlaybackProgressData) -> Unit){
            this@MusicPlaybackService.playbackProgressListener = playbackProgressListener
        }

        fun getSong(songCallback: (song: Song?) -> Unit){
            songCallback(song)
        }

    }

    override fun onBind(intent: Intent?): IBinder? {
        return LocalBinder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            song = intent.getParcelableExtra(SONG)

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager?
            notificationManager?.notify(NOTIFICATION_ID, getNotification(song!!.title))

            start()
        }

        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        startForeground(NOTIFICATION_ID, getNotification("Initializing..."))
    }

    override fun onDestroy() {
        super.onDestroy()
        player?.apply {
            playWhenReady = false
            stop()
            release()

            cache?.release()
            cache = null
        }

    }
    private fun getNotification(name: String) : Notification {
        if (notificationBuilder == null) {
            notificationBuilder = NotificationUtils.instance.getNotification()
        }

        return notificationBuilder!!
            .setContentTitle(getString(R.string.app_name))
            .setContentText(name)
            .setSmallIcon(R.drawable.ic_song_placeholder)
            .build()
    }

    private fun start(){
        if (cache == null) {

            val cacheDir = File(applicationContext.cacheDir, "mimi_audio_cache")
            if (!cacheDir.exists()) {
                cacheDir.mkdirs()
            }

            // TODO: Implement DatabaseProvider usage
            cache = SimpleCache(
                cacheDir,
                LeastRecentlyUsedCacheEvictor(MAX_CACHE_SIZE_IN_BYTES)
            )
        }
        if (song?.streamUrl != null) {
            configurePlayer()
        }
    }

    private fun configurePlayer(){
        player = ExoPlayerFactory.newSimpleInstance(this, DefaultTrackSelector()).apply {
            volume = 1f
            addListener(object : Player.EventListener{
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    updateProgressBar()
                }
            })
            repeatMode = Player.REPEAT_MODE_ALL
            prepareToPlayAudioFromUrl(this@MusicPlaybackService, song!!.streamUrl, cache)
            playWhenReady = true
        }
    }

    private fun updateProgressBar() {
        val duration = player?.duration ?: 0
        val position = player?.currentPosition ?: 0
        val bufferedPosition = player?.bufferedPosition ?: 0

        playbackProgressListener?.invoke(PlaybackProgressData(position, bufferedPosition, duration))

        // Remove scheduled updates.
        progressHandler.removeCallbacks(updateProgressAction)

        // Schedule an update if necessary.
        val playbackState = player?.playbackState ?: Player.STATE_IDLE

        if (playbackState != Player.STATE_IDLE && playbackState != Player.STATE_ENDED) {
            progressHandler.postDelayed(updateProgressAction, 1000)
        }
    }
}