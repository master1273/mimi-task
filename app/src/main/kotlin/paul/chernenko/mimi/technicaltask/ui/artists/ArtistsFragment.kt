package paul.chernenko.mimi.technicaltask.ui.artists

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_artists.*
import paul.chernenko.mimi.technicaltask.MimiApp
import paul.chernenko.mimi.technicaltask.R
import paul.chernenko.mimi.technicaltask.model.Artist
import paul.chernenko.mimi.technicaltask.repository.HearThisRepositoryImpl
import paul.chernenko.mimi.technicaltask.ui.getViewModel
import paul.chernenko.mimi.technicaltask.utils.LiveDataResult

class ArtistsFragment: Fragment() {

    private val viewModel : ArtistsViewModel by lazy {
        getViewModel{ ArtistsViewModel(HearThisRepositoryImpl(MimiApp.apiClient, MimiApp.appContext))
        }
    }

    // Flag to allow fetch artists when the end of the list is reached
    // Depends on the loading state
    private var lockListView: Boolean = false

    private val artistsObserver = Observer<LiveDataResult<List<Artist>>> { result ->
        when (result?.status) {
            LiveDataResult.Status.LOADING -> {
                this.viewModel.setLoadingVisibility(true)
                lockListView = true
            }

            LiveDataResult.Status.ERROR -> {
                showErrorSnackbar(result.err!!.message!!)
                lockListView = true
            }

            LiveDataResult.Status.SUCCESS -> {
                updateList(result.data!!)
                lockListView = false
            }
        }
    }

    private val artistDetailsObserver = Observer<Artist> { artist ->
        updateArtistDetails(artist)
    }

    private val selectedItemPostitonObserver = Observer<Int> { position ->
        (artists_list.adapter as? ArtistsAdapter)?.apply {
            artists_list.scrollToPosition(position)
        }
    }

    private val loadingObserver = Observer<Boolean> { visible ->
        if (visible) {
            (artists_list.adapter as? ArtistsAdapter)?.apply {
                showLoadingIndicator()
                artists_list.smoothScrollToPosition(itemCount - 1)
            }
        } else {
            (artists_list.adapter as? ArtistsAdapter)?.removeLoadingIndicator()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_artists, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // bind observers
        this.viewModel.artistsObserver.observe(this, this.artistsObserver)
        this.viewModel.loadingLiveData.observe(this, this.loadingObserver)
        this.viewModel.artistDetailsObserver.observe(this, this.artistDetailsObserver)
        this.viewModel.selectedArtistPositionObserver.observe(this, this.selectedItemPostitonObserver)

        // configure recycler view
        configureList()

        // Load data
        this.viewModel.fetchArtists()

        // set toolbar title
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Top artists"
    }

    private fun showErrorSnackbar(message: String){
        Snackbar.make(main, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.try_again){
                this.viewModel.fetchArtists()
            }
            .show()
    }

    private fun configureList(list: MutableList<Artist> = mutableListOf()){
        artists_list.layoutManager = LinearLayoutManager(activity)
        artists_list.setHasFixedSize(true)
        artists_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && !lockListView) {
                    viewModel.fetchArtists()
                }
            }
        })
        artists_list.adapter = ArtistsAdapter(list){
            openSongsPage(it)
            viewModel.saveScrollPosition((artists_list.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition())
        }
    }

    private fun updateList(artists: List<Artist>){
        if ((artists_list.adapter as? ArtistsAdapter) != null) {
            (artists_list.adapter as ArtistsAdapter).replaceItems(artists)
            viewModel.loadArtistDetails()
        } else {
            configureList(artists.toMutableList())
        }
    }

    private fun updateArtistDetails(artist: Artist){
        (artists_list.adapter as? ArtistsAdapter)?.updateArtistDetails(artist)
    }

    private fun openSongsPage(artist: Artist){
        val bundle = Bundle()
        bundle.putParcelable("artist", artist)
        findNavController().navigate(R.id.action_artists_dest_to_songs_dest, bundle)
    }
}
