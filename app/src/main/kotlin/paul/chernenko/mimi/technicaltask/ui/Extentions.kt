package paul.chernenko.mimi.technicaltask.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

inline fun <reified T : ViewModel> Fragment.getViewModel(noinline creator: (() -> T)): T {
    return ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(noinline creator: (() -> T)): T {
    return ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}