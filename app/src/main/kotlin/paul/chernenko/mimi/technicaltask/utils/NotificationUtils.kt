package paul.chernenko.mimi.technicaltask.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import paul.chernenko.mimi.technicaltask.BuildConfig
import paul.chernenko.mimi.technicaltask.MimiApp
import paul.chernenko.mimi.technicaltask.R

class NotificationUtils(base: Context): ContextWrapper(base) {

    private object Holder {
        val instance = NotificationUtils(MimiApp.appContext)
    }

    companion object {
        private const val ANDROID_CHANNEL_ID = BuildConfig.APPLICATION_ID
        private val ANDROID_CHANNEL_NAME: String = MimiApp.appContext.resources.getString(R.string.app_name)
        val instance: NotificationUtils by lazy { Holder.instance }
    }

    private val manager: NotificationManager =
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannels()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannels() {
        // create android channel
        val androidChannel = NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_DEFAULT)
        androidChannel.enableLights(false)
        androidChannel.enableVibration(false)
        androidChannel.setSound(null, null)
        androidChannel.vibrationPattern = longArrayOf(300, 300)
        androidChannel.lightColor = Color.GREEN
        androidChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        manager.createNotificationChannel(androidChannel)
    }

    fun getNotification(): Notification.Builder {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification
                .Builder(applicationContext, ANDROID_CHANNEL_ID)
                .setAutoCancel(true)
        } else {
            @Suppress("DEPRECATION")
            Notification
                .Builder(applicationContext)
                .setAutoCancel(true)
        }
    }
}